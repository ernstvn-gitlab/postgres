[![coverage report](https://gitlab.com/gitlab-cog/postgres/badges/master/coverage.svg)](https://gitlab.com/gitlab-cog/postgres/commits/master)

## Design

This project has one simple thing to do:

* Allow GitLab employees to send read only queries to postgresq

The way to do it is like this:

* Using the cog-rb gem we create this project in a way that we have a Cog command that can take an arbitrary number of arguments
* These arguments get joined into a single string, this is our query.
* We get the username, password and host we connect to from ENV variables. This connection data must be a read only connection.
* We create a connection (or use a connection pool) and execute the query.
* We send the result of the query execution as the content as JSON back to the caller.

By doing it like this we can issue any kind of query, explain analyze, select queries, whatever.

This will be deployed with a cog relay on the staging server, only having this bundle, and the queries will be directed to this.

We should start having this in staging and then when we feel confident, with a low statement timeout, we could even have this in the production database to gather data through chatops.

## Running this project

When a pipeline is triggered on master, the `latest` tag is upgrade for the
container we than build. One should deploy that container.

The container has to access postgres, and know how to by the environment
variable `DB_CONNECT_STRING`. Example:

```bash
export DB_CONNECT_STRING="dbname=gitlabhq_development host=/path/to/postgresql"
```
