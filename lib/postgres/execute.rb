require "pg"
require "json"

module Postgres
  class Client
    def execute(query)
      query = connection.escape_string(query)

      begin
        connection.exec("BEGIN READ ONLY")
        connection.exec("SET statement_timeout TO 1000")
        values = connection.exec(query).values
        connection.exec("COMMIT")

        [values, nil]
      rescue PG::Error => e
        [[], e]
      end
    end

    private

    def connection
      @connection ||= PG.connect(ENV["DB_CONNECT_STRING"])
    end
  end
end
